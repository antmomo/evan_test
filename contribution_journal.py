#!/usr/bin/python
# -*- coding: UTF-8 -*-
import pdb
import con_journal_xml
# from con_journal_xml import create_xml_test
from openpyxl import load_workbook

def get_value(i,ws,article_row):
    value=ws.cell(row=article_row,column=i).value
    i+=1
    return value,i
def geneList(f):
    wb = load_workbook(filename=f, read_only=True)
    ws = wb.worksheets[0]
    max_row = ws.max_row
    info_list=[]
    for ar_row in range(2,max_row+1):
        i=3
        sourceID,i=get_value(i,ws,ar_row)
        pv,i=get_value(i,ws,ar_row)
        pubtype,i=get_value(i,ws,ar_row)
        date,i=get_value(i,ws,ar_row)
        lang,i=get_value(i,ws,ar_row)
        titles,i=get_value(i,ws,ar_row)
        subtitles,i=get_value(i,ws,ar_row)
        abstracts,i=get_value(i,ws,ar_row)
        pages,i=get_value(i,ws,ar_row)
        no_of_pages,i=get_value(i,ws,ar_row)
        article_no,i=get_value(i,ws,ar_row)
        issue,i=get_value(i,ws,ar_row)
        volume,i=get_value(i,ws,ar_row)
        authors_aff,i=get_value(i,ws,ar_row)
        manage_org,i=get_value(i,ws,ar_row)
        journal,i=get_value(i,ws,ar_row)
        DOI,i=get_value(i,ws,ar_row)
        url,i=get_value(i,ws,ar_row)
        Keyword,i=get_value(i,ws,ar_row)
        Event,i=get_value(i,ws,ar_row)
        visibility,i=get_value(i,ws,ar_row)
        ext_ids,i=get_value(i,ws,ar_row)
        Report_to_RGC,i=get_value(i,ws,ar_row)
        uuid,i=get_value(i,ws,ar_row)
        source_id,i=get_value(i,ws,ar_row)
        view_scopus_metrics,i=get_value(i,ws,ar_row)
        uuid_empty,i=get_value(i,ws,ar_row)
        event_id,i=get_value(i,ws,ar_row)
        item_dict={'sourceID':sourceID,'pv':pv,'pubtype':pubtype,
                   'date':date,'lang':lang,'titles':titles,'subtitles':subtitles,'abstracts':abstracts,
                   'pages':pages,'no_of_pages':no_of_pages,'article_no':article_no,'issue':issue,'volume':volume,
                   'authors_aff':authors_aff,'manage_org':manage_org,'journal':journal,'DOI':DOI,'url':url,
                   'Keyword':Keyword,'Event':Event,'visibility':visibility,'ext_ids':ext_ids,'Report_to_RGC':Report_to_RGC,
                   'uuid':uuid,'source_id':source_id,'view_scopus_metrics':view_scopus_metrics,
                   'uuid_empty':uuid_empty,'event_id':event_id}
        info_list.append(item_dict)
    return info_list
if __name__=='__main__':
    info_list=geneList('ConToJournalScopus20170228.xlsx')
#     create_xml_test('contri_journal_evan.xml',info_list)
